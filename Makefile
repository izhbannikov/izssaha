CXX = g++
CFLAGS = -Wall -g 


all:  util.o iz_SSAHA.o pairwise.o main.o izssaha 
		
					
izssaha :   main.o pairwise.o iz_SSAHA.o util.o
	$(CXX) $(CFLAGS)  -o  izssaha main.o pairwise.o iz_SSAHA.o util.o -lpthread -Xlinker -zmuldefs
	
main.o :  
	$(CXX) $(CFLAGS)  -c main.cpp 

pairwise.o :  
	$(CXX) $(CFLAGS)  -c pairwise.cpp
	
iz_SSAHA.o :
	$(CXX) $(CFLAGS)  -c iz_SSAHA.cpp
	
util.o :
	$(CXX) $(CFLAGS)  -c util.cpp

clean:
	rm -rf izssaha *.o
