#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include "timer.h"
#include "util.h"
#include <streambuf>
#include <exception>
#include "iz_SSAHA.h"

using namespace std;

int main(int argc, char *argv[]) {
    double start, finish, elapsed;
    
    GET_TIME(start);	
    
    /*Test iz_ssaha*/
    iz_SSAHA *izssaha = new iz_SSAHA();
    
    string ref_string = "GACTACTATACGAGTGGATCTCCTTCCCGGTTAGTTGTGTTCTCCCCCATTGGGTTCCGGGAGAANNNNNNNNNNNNN";
    string query_string = "TTCCCGGTTAGTTGTG";
    izssaha->Find(ref_string, query_string );
   
    GET_TIME(finish);
    elapsed = finish - start;
    printf("Elapsed time = %e seconds\n", elapsed);
    
    start = finish = elapsed = 0;    
        
    return 0;
}



